//
//  deque.h
//  deque-circulaire
//
//  Jean Goulet 2022-01-26
//  Copyleft 2022 UdeS
//

#ifndef deque_h
#define deque_h

#include <iostream>
#include <string>
#include <cassert>
#include <stdexcept>

template <typename TYPE>
class deque{
private:
    TYPE**  DEBUT;
    size_t CAP,SIZE,ZERO;
public:
    //fonctions de base
    deque(size_t=0);
    ~deque();
    deque(const deque&);
    deque& operator=(const deque&);
    void swap(deque&);
    
    //selecteurs
    size_t size()const;
    bool empty()const;
    
    //déclaration des classes d'iteration
    class iterator;
    class reverse_iterator;
    
    iterator begin();
    iterator end();
    reverse_iterator rbegin();
    reverse_iterator rend();
    
    //gestion de la memoire
    void resize(size_t);  //change la dimension
    void reserve(size_t); //augmente la capacite
    void clear();         //vide la memoire
    
    //acces aux elements individuels
    TYPE& back();
    const TYPE& back()const;
    TYPE& front();
    const TYPE& front()const;

    TYPE& operator[](size_t);
    TYPE& at(size_t);
    const TYPE& operator[](size_t)const;
    const TYPE& at(size_t)const;
    void push_back(const TYPE&);
    void pop_back();
    void push_front(const TYPE&);
    void pop_front();
    
    //code de mise au point
    void afficher(std::string="")const;
};

//fonctions de base

template <typename TYPE>
deque<TYPE>::deque(size_t D){
    SIZE=CAP=D;
    ZERO=0;
    if(D>0)DEBUT=new TYPE*[CAP];
    else DEBUT=nullptr;
    for(size_t i=0;i<D;++i)DEBUT[i]=new TYPE(TYPE());
}

template <typename TYPE>
deque<TYPE>::~deque(){
    clear();
}

template <typename TYPE>
deque<TYPE>& deque<TYPE>::operator=(const deque& source){
    if(this!=&source){
        deque copie(source);
        swap(copie);
    }
    return *this;
}

template <typename TYPE>
void deque<TYPE>::swap(deque& source){
    std::swap(DEBUT,source.DEBUT);
    std::swap(CAP,source.CAP);
    std::swap(SIZE,source.SIZE);
    std::swap(ZERO,source.ZERO);
}



//code jetable
//pour la mise au point

template <typename TYPE>
void deque<TYPE>::afficher(std::string S)const{
    using namespace std;
    cout<<endl<<"---"<<S<<"------------------------"<<endl;
    cout<<"DEBUT:"<<DEBUT<<endl;
    cout<<"CAP:  "<<CAP<<endl;
    cout<<"SIZE: "<<SIZE<<endl;
    cout<<"ZERO: "<<ZERO<<endl;
    cout<<"---"<<string(S.size(),'-')<<"------------------------"<<endl;
    size_t i,j;
    for(i=0;i<CAP;++i){
        j=(CAP+i-ZERO)%CAP;
        if(j>=SIZE)cout<<i<<"(-)"<<endl;
        else cout<<i<<"("<<j<<"): "<<*DEBUT[i]<<endl;
    }
    if(CAP>0)cout<<"---"<<string(S.size(),'-')<<"------------------------"<<endl;
}

template <typename TYPE>
std::ostream& operator<<(std::ostream& out,const deque<TYPE>& V){
    char delim='[';
    size_t S=V.size();
    if(S==0)out<<delim;
    if(S>40)S=40;
    for(size_t i=0;i<S;++i){
        out<<delim<<V[i];
        delim=',';
    }
    out<<"]";
    return out;
}

//class iterator et reverse_iterator

template<typename TYPE>
class deque<TYPE>::iterator{
friend class deque<TYPE>;
private:
    deque<TYPE>* leDEQUE;
    TYPE **POINTEUR;
    TYPE**  POINTEUR_NULL=nullptr;

public:
    iterator(deque<TYPE>* L=nullptr,TYPE **P=nullptr):leDEQUE(L),POINTEUR(P){}
    TYPE& operator*();
    iterator& operator++();     //++i
    iterator operator++(int);   //i++
    iterator& operator--();     //--i
    iterator operator--(int);   //i--
    bool operator==(const iterator& droite){
        return POINTEUR==droite.POINTEUR &&
        leDEQUE==droite.leDEQUE;
    }
    bool operator!=(const iterator& droite){
        return POINTEUR!=droite.POINTEUR ||
        leDEQUE!=droite.leDEQUE;
    }
};

template<typename TYPE>
class deque<TYPE>::reverse_iterator{
friend class deque<TYPE>;
private:
    deque<TYPE>* leDEQUE;
    TYPE **POINTEUR;
public:
    reverse_iterator(deque<TYPE>* L=nullptr,TYPE **P=nullptr):leDEQUE(L),POINTEUR(P){}
    TYPE& operator*();
    reverse_iterator& operator++();     //++i
    reverse_iterator operator++(int);   //i++
    reverse_iterator& operator--();     //--i
    reverse_iterator operator--(int);   //i--
    bool operator==(const reverse_iterator& droite){
        return POINTEUR==droite.POINTEUR &&
        leDEQUE==droite.leDEQUE;
    }
    bool operator!=(const reverse_iterator& droite){
        return POINTEUR!=droite.POINTEUR ||
        leDEQUE!=droite.leDEQUE;
    }
};



#include "deque2.h"


#endif /* deque_h */
