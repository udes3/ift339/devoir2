#include <iostream>
#include "deque.h"

using namespace std;

int main() {
	deque<double>D(5);
	D[0] = 1; D[2] = 3; D[4] = 4;
	D.afficher("Test");
	//deque<double>DA(D);
	//DA.afficher("Copie");
	//D.reserve(10);
	//D.resize(7);
	//D.afficher("resize 7");
	//DA.clear();
	//DA.afficher("Clear copie");
	//D.push_back(-4);
	D.push_front(9);
	//cout << "\nFront : " << D.front();
	//cout << "\nBack : " << D.back();
	D.afficher("Deque");	
	deque<double>::reverse_iterator it;
	it = D.rend();
	--it;
	--it;
	cout << *it;
	for (it = D.rbegin(); it != D.rend(); it++)
	{
		cout << *it << " ";
	}

}