//
//  deque2.h
//  deque-circulaire
//
//  Jean Goulet 2022-01-26
//  Copyleft 2016 UdeS
//

#ifndef deque2_h
#define deque2_h

#include <iostream>
#include <string>
#include <cassert>
#include <stdexcept>

//fonctions de base
//copieur
template <typename TYPE>
deque<TYPE>::deque(const deque<TYPE>& source):deque(source.size()){
    if (CAP > 0)
    {
        for (size_t i = 0; i < SIZE; ++i) {
            *DEBUT[i] = *source.DEBUT[(i + source.ZERO) % source.CAP];
        }
    }
    else { DEBUT = nullptr; }
}

//selecteurs
template <typename TYPE>
size_t deque<TYPE>::size()const{return SIZE;}

template <typename TYPE>
bool deque<TYPE>::empty()const{return SIZE==0;}

//gestion de la mémoire
template <typename TYPE>
void deque<TYPE>::resize(size_t nSIZE){
    if (nSIZE < SIZE)
    {
        for (size_t i = SIZE; i > nSIZE; --i) {
            delete DEBUT[(i - 1) % CAP];
        }
        SIZE = nSIZE;
        return;
    }
    if (nSIZE > CAP) {
        reserve(nSIZE);
    }
    for (size_t i = SIZE; i < nSIZE; ++i) {
        DEBUT[i] = new TYPE(TYPE());
    }
    SIZE = nSIZE;        
}

template <typename TYPE>
void deque<TYPE>::reserve(size_t nCAP){
    if (CAP < nCAP) {
        TYPE** tmp = DEBUT;
        DEBUT = new TYPE * [nCAP];
        for(int i = 0; i < SIZE ; ++i){
            DEBUT[i] = tmp[(i + ZERO) % CAP];
        }
        CAP = nCAP;
        delete tmp;
    }
 }

template <typename TYPE>
void deque<TYPE>::clear(){
    for (size_t i = 0; i < SIZE; i++)
    {
        delete DEBUT[(i + ZERO) % CAP];
    }
    delete[] DEBUT;
    DEBUT = nullptr;
    SIZE = CAP = ZERO = 0;
 }

//accès à un élément individuel

template <typename TYPE>
TYPE& deque<TYPE>::back() { return *DEBUT[(ZERO + SIZE - 1) % CAP]; }

template <typename TYPE>
const TYPE& deque<TYPE>::back()const { return *DEBUT[(ZERO + SIZE - 1) % CAP]; }

template <typename TYPE>
TYPE& deque<TYPE>::front() { return *DEBUT[ZERO]; }

template <typename TYPE>
const TYPE& deque<TYPE>::front()const { return *DEBUT[ZERO]; }

template <typename TYPE>
TYPE& deque<TYPE>::operator[](size_t i){
    return *DEBUT[(i + ZERO) % CAP];
}

template <typename TYPE>
const TYPE& deque<TYPE>::operator[](size_t i)const{
    return *DEBUT[(i + ZERO) % CAP];
}

template <typename TYPE>
TYPE& deque<TYPE>::at(size_t i){
    if (i >= SIZE || i<0) {
        throw std::out_of_range("index out of range");
    }
    return *DEBUT[(i + ZERO) % CAP];
}

template <typename TYPE>
const TYPE& deque<TYPE>::at(size_t i)const{
    if (i >= SIZE || i<0) {
        throw std::out_of_range("index out of range");
    }
    return *DEBUT[(i + ZERO) % CAP];
}

template <typename TYPE>
void deque<TYPE>::push_back(const TYPE& VAL){
    if (SIZE == CAP) reserve(CAP * 2 + 1);
    DEBUT[(SIZE + ZERO) % CAP] = new TYPE(VAL);
    SIZE++;
}


template <typename TYPE>
void deque<TYPE>::pop_back(){
    if (SIZE != 0) {
        delete DEBUT[(ZERO + SIZE - 1) % CAP];
        DEBUT[(ZERO + SIZE - 1) % CAP] = nullptr;
        --SIZE;
        if (SIZE == 0)clear();
    }
}

template <typename TYPE>
void deque<TYPE>::push_front(const TYPE& VAL){
    if (SIZE == CAP) reserve(CAP * 2 + 1);
    if (ZERO == 0) ZERO = CAP - 1;
    else {
        ZERO = ZERO - 1; 
    }
    DEBUT[ZERO] = new TYPE(VAL);
    SIZE++;
}

template <typename TYPE>
void deque<TYPE>::pop_front(){
    if (SIZE != 0) {
        delete DEBUT[ZERO];
        DEBUT[ZERO] = nullptr;
        ZERO = (ZERO + 1 + CAP) % CAP;
        SIZE--;
        if (SIZE == 0)clear();
    }
}


//fonctions d'iteration de la class deque<TYPE>

template <typename TYPE>
typename deque<TYPE>::iterator deque<TYPE>::begin(){
    iterator retour(this, DEBUT + ZERO);
    return retour;
    }

template <typename TYPE>
typename deque<TYPE>::iterator deque<TYPE>::end(){
    if (CAP == 0) {
        iterator retour = this->begin();
        return retour;
    }
    iterator retour(this, DEBUT + ((ZERO + SIZE) % CAP));
    return retour;
    }

template <typename TYPE>
typename deque<TYPE>::reverse_iterator deque<TYPE>::rbegin(){
    if (CAP == 0) {
        reverse_iterator retour = this->rend();
        return retour;
    }
    reverse_iterator retour(this, DEBUT + ((ZERO + SIZE) % CAP));
    return retour;
}

template <typename TYPE>
typename deque<TYPE>::reverse_iterator deque<TYPE>::rend(){
    reverse_iterator retour(this, DEBUT + ZERO);
    return retour;
}

//fonction de la class deque<TYPE>::iterator

template <typename TYPE>
TYPE& deque<TYPE>::iterator::operator*(){
    return **POINTEUR;
}

template <typename TYPE>
typename  deque<TYPE>::iterator& deque<TYPE>::iterator::operator++(){    //++i
    ++POINTEUR;
    if (POINTEUR == leDEQUE->DEBUT + leDEQUE->CAP && POINTEUR != leDEQUE->DEBUT + leDEQUE->ZERO+leDEQUE->SIZE)
        POINTEUR = leDEQUE->DEBUT;
    return *this;
}

template <typename TYPE>
typename deque<TYPE>::iterator deque<TYPE>::iterator::operator++(int){  //i++
    iterator tmp = iterator(*this);
    this->operator++();
    return tmp;
}

template <typename TYPE>
typename  deque<TYPE>::iterator& deque<TYPE>::iterator::operator--(){     //--i
    if (POINTEUR == leDEQUE->DEBUT)
        POINTEUR = leDEQUE->DEBUT+leDEQUE->CAP;
    --POINTEUR;
    return *this;
}

template <typename TYPE>
typename deque<TYPE>::iterator deque<TYPE>::iterator::operator--(int){  //i--
    iterator tmp = iterator(*this);
    this->operator--();
    return tmp;
}

//fonction de la class deque<TYPE>::reverse_iterator

template <typename TYPE>
TYPE& deque<TYPE>::reverse_iterator::operator*(){
    if (POINTEUR == leDEQUE->DEBUT)
        POINTEUR = leDEQUE->DEBUT + leDEQUE->CAP;
    return **(POINTEUR - 1);
}

template <typename TYPE>
typename  deque<TYPE>::reverse_iterator& deque<TYPE>::reverse_iterator::operator++(){    //++i
    if (POINTEUR == leDEQUE->DEBUT)
        POINTEUR = leDEQUE->DEBUT + leDEQUE->CAP;
    --POINTEUR;
    return *this;
}

template <typename TYPE>
typename deque<TYPE>::reverse_iterator deque<TYPE>::reverse_iterator::operator++(int){  //i++
    reverse_iterator tmp = reverse_iterator(*this);
    this->operator++();
    return tmp;
}

template <typename TYPE>
typename  deque<TYPE>::reverse_iterator& deque<TYPE>::reverse_iterator::operator--(){     //--i
    if (POINTEUR == leDEQUE->DEBUT + leDEQUE->CAP)
        POINTEUR = leDEQUE->DEBUT;
    ++POINTEUR;
    return *this;
}

template <typename TYPE>
typename deque<TYPE>::reverse_iterator deque<TYPE>::reverse_iterator::operator--(int){  //i--
    reverse_iterator tmp = reverse_iterator(*this);
    this->operator--();
    return tmp;

}


#endif /* deque2_h */


